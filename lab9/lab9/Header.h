#include <iostream>
using namespace std;
class Tovar {	
	double cen; 
	double kol; 
	char* kat; 
	char* name;
	public:
		Tovar();
	Tovar(double Cen,double Kol, char* Kat, char* Name);
	double getCen();
	double getKol();
	char* getKat();
	char* getName();
	friend istream& operator >> (istream& in, Tovar& tovar);
	friend ostream& operator << (ostream& out, Tovar& tovar);
};
void selectSort1(Tovar* arr, int n);
void selectSort2(Tovar* arr, int n);
void merge1(Tovar* arr, int n);
void merge2(Tovar* arr, int n);