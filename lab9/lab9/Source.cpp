# include "Header.h"
# include "iostream"
Tovar::Tovar() {
	cen = 0;
	kol = 0;
	kat = new char[100];
	name = new char[100];
}
Tovar::Tovar(double Cen, double Kol, char* Kat, char* Name) {
	cen = Cen;
	kol = Kol;
	kat = Kat;
	name = Name;
}
double Tovar::getCen() {
	return cen;
}
double Tovar::getKol() {
	return kol;
}
char* Tovar::getKat() {
	return kat;
}
char* Tovar::getName() {
	return name;
}
istream& operator >> (istream& in, Tovar& tovar) {
	in >> tovar.cen >> tovar.kol >> tovar.kat >> tovar.name;
	return in;
}
ostream& operator << (ostream& out, Tovar& tovar) {
	out << tovar.cen << " " << tovar.kol << " " << tovar.kat << " " << tovar.name;
	return out;
}
void selectSort1(Tovar* arr, int n)
{
    //pos_min is short for position of min
    int pos_min;
    Tovar temp;

    for (int i = 0; i < n - 1; i++)
    {
        pos_min = i;//set pos_min to the current index of array

        for (int j = i + 1; j < n; j++)
        {

            if (arr[j].getKol() > arr[pos_min].getKol())
                pos_min = j;
            //pos_min will keep track of the index that min is in, this is needed when a swap happens
        }

        //if pos_min no longer equals i than a smaller value must have been found, so a swap must occur
        if (pos_min != i)
        {
            temp = arr[i];
            arr[i] = arr[pos_min];
            arr[pos_min] = temp;
        }
    }
}
void selectSort2(Tovar* arr, int n)
{
    //pos_min is short for position of min
    int pos_min;
    Tovar temp;

    for (int i = 0; i < n - 1; i++)
    {
        pos_min = i;//set pos_min to the current index of array

        for (int j = i + 1; j < n; j++)
        {
            if (strcmp(arr[j].getKat(), arr[pos_min].getKat()) == -1) {
                pos_min = j;
            }
            else if (strcmp(arr[j].getKat(), arr[pos_min].getKat()) == 0) {
                if (arr[j].getCen() < arr[pos_min].getCen())
                    pos_min = j;
            }
 
            //pos_min will keep track of the index that min is in, this is needed when a swap happens
        }

        //if pos_min no longer equals i than a smaller value must have been found, so a swap must occur
        if (pos_min != i)
        {
            temp = arr[i];
            arr[i] = arr[pos_min];
            arr[pos_min] = temp;
        }
    }
}
void merge1(Tovar* arr, int n)
    {
        double mid = n / 2;
        if (n % 2 == 1)
            mid++;
        int h = 1;
        Tovar* c = (Tovar*)malloc(n * sizeof(int));
        int step;
        while (h < n)
        {
            step = h;
            int i = 0;
            int j = mid;
            int k = 0;
            while (step <= mid)
            {
                while ((i < step) && (j < n) && (j < (mid + step)))
                {
                    if (arr[i].getKol() < arr[j].getKol())
                    {
                        c[k] = arr[i];
                        i++; k++;
                    }
                    else {
                        c[k] = arr[j];
                        j++; k++;
                    }
                }
                while (i < step)
                {
                    c[k] = arr[i];
                    i++; k++;
                }
                while ((j < (mid + step)) && (j < n))
                {
                    c[k] = arr[j];
                    j++; k++;
                }
                step = step + h;
            }
            h = h * 2;
            for (int i = 0; i < n; i++)
                arr[i] = c[i];
        }
     
    }
void merge2(Tovar* arr, int n)
    {
    //pos_min is short for position of min
    int pos_min;
    Tovar temp;

    for (int i = 0; i < n - 1; i++)
    {
        pos_min = i;//set pos_min to the current index of array

        for (int j = i + 1; j < n; j++)
        {

            if (arr[j].getKol() > arr[pos_min].getKol())
                pos_min = j;
            //pos_min will keep track of the index that min is in, this is needed when a swap happens
        }

        //if pos_min no longer equals i than a smaller value must have been found, so a swap must occur
        if (pos_min != i)
        {
            temp = arr[i];
            arr[i] = arr[pos_min];
            arr[pos_min] = temp;
        }
    }
}